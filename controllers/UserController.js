const User = require("../models/User")
const Course = require("../models/Course")
const auth = require('../auth')
const bcrypt = require("bcrypt")

module.exports.checkEmailExists = (request_body) => {
	return User.find({email: request_body.email}).then(result => {
		if(result.length > 0){
			return true 
		} 

		return false
	})
}

module.exports.registerUser = (request_body) => {
	let new_user = new User({
		firstName: request_body.firstName,
		lastName: request_body.lastName,
		email: request_body.email,
		mobileNo: request_body.mobileNo,
		password: bcrypt.hashSync(request_body.password, 10) //hashSync function will 'hash' the password and turn in into random characters. The second argument serves as the amount of times that the password will be hashed. Note: The higher the number, the slower it is to 'unhash' which will affect application performance, and the lower the number the faster it will be.
	})

	return new_user.save().then((registered_user, error) => {
		if(error){
			return error 
		} 

		return 'User successfully registered!'
	})
}

module.exports.loginUser = (request_body) => {
	return User.findOne({email: request_body.email}).then(result => {
		if(result === null){
			return "The user doesn't exist."
		}

		// We can't use regular comparison to check if password is correct because the password of the existing user is hashed with bcrypt. We have to use bcrypt to de-hash the password and compare it to the password from the request body. The compareSync() function will return either true or false depending on if they match.
		const is_password_correct = bcrypt.compareSync(request_body.password, result.password)

		if(is_password_correct){
			return {
				// Using the createAccessToken function, we can generate a token using the user data after its password has been validated.
				accessToken: auth.createAccessToken(result.toObject())
			}
		}

		return 'The email and password combination is not correct!'
	})
}

// For getting user details from the token
module.exports.getProfile = (userId) => {
	return User.findById(userId).then(result => {
		return result
	})
}

// For enrolling a user to a course
module.exports.enroll = async (request_body) => {
	let userSaveStatus = await User.findById(request_body.user_id).then(user => {
		user.enrollments.push({courseId: request_body.course_id})

		return user.save().then((user,error) => {
			if(error){
				return false
			}

			// Returns true if the enrollments field has added a new course inside
			return true
		})
	})

	let courseSaveStatus = await Course.findById(request_body.course_id).then (course => {
		course.enrollees.push({userId: request_body.user_id})

		return course.save().then((course,error) => {
			if(error){
				return false
			}

			return true
		})
	})

	if(userSaveStatus && courseSaveStatus){
		return {
			message: 'User has enrolled successfully!'
		}
	} else {
		return {
			message: 'Something went wrong.'
		}
	}
}