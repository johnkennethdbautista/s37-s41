const User = require("../models/User")
const Course = require("../models/Course")
const auth = require('../auth')
const bcrypt = require("bcrypt")

// Get all courses
module.exports.getAllCourse = () => {
	return Course.find().then(all_course => {
		return all_course
	})
}

// Archiving the course
module.exports.archiveCourse = (course_id) => {
	let archive_course = {
		isActive: false
	}

	return Course.findByIdAndUpdate(course_id, archive_course).then((modified_course, error) => {
		if(error){
			return error
		}

		return {
			message: 'Course has been put into archive.',
			data: modified_course
		}
	})
}

module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(active_courses => {
		return active_courses
	})
}

module.exports.createCourse = (request_body) => {
	let new_course = new Course({
		name: request_body.name,
		description: request_body.description,
		price: request_body.price
	})

	return new_course.save().then((created_course, error) => {
		if(error){
			return error
		}

		return {
			message: "Course created successfully!",
			data: created_course
		}
	})
}

// Get single course
module.exports.getCourse = (course_id) => {
	return Course.findById(course_id).then(result => {
		return result
	})
}

// Update existing course
module.exports.updateCourse = (course_id, request_body) => {
	let updated_course = {
		name: request_body.name,
		description: request_body.description,
		price: request_body.price,
		isActive: request_body.isActive
	}

	return Course.findByIdAndUpdate(course_id, updated_course).then((modified_course, error) => {
		if(error){
			return error
		}

		return {
			message: 'Course updated successfully!',
			data: modified_course
		}
	})
}