const express = require('express')
const router = express.Router()
const CourseController = require('../controllers/CourseController')
const auth = require('../auth')

// Get all ACTIVE courses
router.get('/active-courses', (request, response) => {
	CourseController.getAllActive().then(result => response.send(result))
})

// Create new course
router.post('/', auth.verify, (request, response) => {
	CourseController.createCourse(request.body).then(result => response.send(result))
})

// Get single course
router.get('/:id', (request, response) => {
	CourseController.getCourse(request.params.id).then(result => response.send(result))
})

// Update details of existing course
router.patch('/:id/update', auth.verify, (request, response) => {
	CourseController.updateCourse(request.params.id, request.body).then(result => response.send(result))
})

// Get all courses
router.get('/', (request, response) => {
	CourseController.getAllCourse().then(result => response.send(result))
})

// Archive course
router.patch('/:id/archive-course', (request, response) => {
	CourseController.archiveCourse(request.params.id).then(result => response.send(result))
})

module.exports = router